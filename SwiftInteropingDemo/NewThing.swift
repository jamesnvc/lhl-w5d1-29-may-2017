//
//  NewThing.swift
//  SwiftInteropingDemo
//
//  Created by James Cash on 29-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit

class NewThing: Thingy {

    func hello() {
        print("Value is \(whatever)!")
    }
}
