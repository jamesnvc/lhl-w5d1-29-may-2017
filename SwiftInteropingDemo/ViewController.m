//
//  ViewController.m
//  SwiftInteropingDemo
//
//  Created by James Cash on 29-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "Thingy.h"
#import "SwiftInteropingDemo-Swift.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Thingy *t1 = [[Thingy alloc] init];
    t1.whatever;

    NewThing *t2 = [[NewThing alloc] init];
    t2.whatever;
    [t2 hello];
    //[[Whatever alloc] init];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
