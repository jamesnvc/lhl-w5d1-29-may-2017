//
//  Thingy.h
//  SwiftInteropingDemo
//
//  Created by James Cash on 29-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Thingy : NSObject

@property (nonatomic,strong) NSString *whatever;

@end

/*
NSArray<NSString*>* foo;
NSArray* quux;
*/
