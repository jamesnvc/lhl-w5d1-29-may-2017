//
//  File.swift
//  SwiftInteropingDemo
//
//  Created by James Cash on 29-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import Foundation

func foo() {
    print("Hello world!")
}

class Whatever /*: NSObject*/ {
    func bar() {
        print("Pure swift!")
    }
}

// Syntax sugar
struct Aoeu {
    // All equivalent
    var bar: [String]
    var bar2: Array<String>
    var bar3: Array<Any>

    // No more primitive types!
    var bar4: [Int]

    // All equivalent
    var quux: String?
    var quux2: Optional<String>
}

enum MyOptional<T> {
    case Nothing
    case Something(val: T)
}

